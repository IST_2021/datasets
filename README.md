## Crowdsourced test report prioritization considering bug severity

---

**Folder "reports"**: it contains raw crowdsourced test reports on six projects.

---

**Folder "results"**: it describes the experiment results, including the APFD values, APFD
values considering bug severity, and MAE values. Each file has 30 rows, and each row is an
experimental result. In addition, each column is the results of different comparison methods,
ranging from RANDOM, BDDiv, DMBD19, TSE20, to CTRP.

---

**Folder "src"**: it contains the source code.



- SPM(in Matlab): contains Spatial Pyramid Matching(SPM), scale-invariant feature
  transform(SIFT) descriptors extraction, histograms building, and histogram distance
  calculation for screenshots



- e2lsh: the locality-sensitive hashing technique (LSH) to map the image feature vector to an
  index value



- baseline_bddiv.py & baseline_dmbd19.py & baseline_tse20.py & baseline_ctrp.py:
  main functions



- bddiv.py & clustering.py & lsh_approach.py: feature extractor of baseline methods



- metric.py: the metrics



- model.py & read_data.py: data reading and report object generation



- stopwords1893.txt: the stopwords list

---

**Folder "statistical_analysis"**: it contains the python scripts to analyze the experimental data.



- normal_test.py: normality test



- statistic_tests.py: Kruskal-Wallis test, Wilcoxon signed-rank test, and Cohen's d


---

## Requirements

Matlab:

1. run the script "mySPMain.m" in the folder "SPM".

2. extract the image feature in the file "pyramids_all_200_3.mat".

3. obtain the image distance matrix in the file "SPDistanceMatrix-**.txt".