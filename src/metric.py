# -*- coding: UTF-8 -*-
from collections import Counter

import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_absolute_error


def pyplot(caselist, row_index):
    x_ = [float(i) / len(caselist) for i in range(len(caselist) + 1)]
    y_ = [0.0] * len(x_)
    row_index.sort()

    for i, value in enumerate(row_index):
        y_[value] = (i + 1) / len(row_index)

    for i, value in enumerate(y_):
        if value == 0:
            y_[i] = y_[i - 1]

    plt.xlim(0, 1)
    plt.ylim(0, 1)
    plt.plot(x_, y_)
    # plt.show()

    linear_interpolation = []
    for i in np.linspace(0.25, 1, 4):
        if i in y_:
            linear_interpolation.append(x_[y_.index(i)] * len(caselist))
        else:
            linear_interpolation.append(np.interp(i, y_, x_) * len(caselist))
    return linear_interpolation


def apfd(Q, caselist, n, M):
    fault_index = {}
    for index in Q:
        case = caselist[index]
        if len(fault_index) == M:
            break
        if case.bug_category not in fault_index and case.bug_category is not 'none':
            fault_index[case.bug_category] = Q.index(index)
    print(len(fault_index), fault_index)

    apfd = 1 + float(0.5 / n)
    for key, value in fault_index.items():
        apfd -= float(value / (n * M))
    print('apfd:', apfd)
    print('\n')
    linear_interpolation = pyplot(caselist, list(fault_index.values()))
    return apfd, linear_interpolation


# mean absolute error
def severity_error(caselist, Q):
    bug_severity = []
    for index in Q:
        bug_severity.append(int(caselist[index].severity))

    ideal = []
    temp = Counter(bug_severity)
    for i in range(5, 0, -1):
        for j in range(temp[i]):
            ideal.append(i)
    mae = mean_absolute_error(bug_severity, ideal)
    return mae


def apfd_s5(Q, caselist, n, M):
    M = 0
    fault_index = {}
    for index in Q:
        case = caselist[index]
        if case.bug_category not in fault_index and int(case.severity) >= 4:
            fault_index[case.bug_category] = Q.index(index)


    print(len(fault_index), fault_index)
    M = len(fault_index)

    apfd = 1 + float(0.5 / n)
    for key, value in fault_index.items():
        apfd -= float(value / (n * M))
    print('apfd_s5:', apfd)

    linear_interpolation = pyplot(caselist, list(fault_index.values()))
    print(linear_interpolation)
    print('\n')

    return apfd, linear_interpolation
